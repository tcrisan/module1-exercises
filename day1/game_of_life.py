# Exemplu Conway's game of life
# =============================
# Game of life e un automat celular.
# Anumite reguli se executa pentru fiecare pozitie dintr-un grid
# O pozitie poate fi vie sau moarta.
# . . . .
# . . o .
# . . . o
# . o o o
# O celula se naste traieste si moare in functie de cati din vecini sunt vii
# v v v   <-- vecinii
# v . v
# v v v
# Any live cell with fewer than two live neighbours dies
# Any live cell with two or three live neighbours lives
# Any live cell with more than three live neighbours dies
# Any dead cell with exactly three live neighbours becomes a live cell


# Alegem structura de date cea mai simpla: o lista de linii, unde o linie e o lista de bool
# la 1, 2 avem o celula vie grid[1][2] == True

import turtle
import time
from pprint import pprint

wn = turtle.Screen()
t = turtle.Turtle()

#TODO: first implement the tests!


def neighbour_population(grid, i, j):
    count = 0

    for hor in [-1, 0, 1]:
        for ver in [-1, 0, 1]:
            if not hor == ver == 0 and (0 <= i + hor < 16 and 0 <= j + ver < 16):
                count += 1 if grid[(j + ver) % 16][(i + hor) % 16] else 0

    return count


def kernel(grid, i, j):
    pass


def step(grid):
    next_grid = [[False] * len(l) for l in grid]

    for i, line in enumerate(grid):
        for j, col in enumerate(line):
            neighbours = neighbour_population(grid, i, j)
            previous_state = col
            should_live = 'o' if neighbours == 3 or (neighbours == 2 and previous_state == True) else '.'
            next_grid[i][j] = should_live

    return next_grid

def parse_grid(s):
    grid = []
    for line in s.split('\n'):
        if line == '':
            continue
        cols = []
        for col in line.split(' '):
            cols.append(col)
        grid.append(cols)

    # pprint(grid)
    return grid


def create_cell(x, y, cell_type):
    if cell_type:
        t.color('green')
        t.shape("circle")
    else:
        t.color('red')
        t.shape("turtle")
    t.goto(x, y)
    t.stamp()


def print_grid(grid):
    t.clear()
    x_origin = 100
    y_origin = 100
    multiplier = 30
    for i, line in enumerate(grid):
        for j, col in enumerate(line):
            if col == 'o':
                create_cell(y_origin + j * multiplier, x_origin - i * multiplier, True)
            else:
                create_cell(y_origin + j * multiplier, x_origin - i * multiplier, False)


def console_tui(initial_grid):
    grid = initial_grid
    while True:
        print_grid(grid)
        grid = step(grid)
        print_grid(grid)

def click_the_turtle():
    t.stamp()

test_grid = parse_grid("""
. . . . . . . . . . . . . . . .
. . . . . o . . . . . . . . . .
. . . . . . o . . . . . . . . .
. . . . o o o . . . . . . . . . 
. . . . . . . . . . . . . . . .
. . . . . . . . . . . . . . . .
. . . . . . . o . . . . . . . .
. . o . . . . . . . . . . . . . 
. . o o . . . . o . . . . . . .
. . o . . . . o . . . . . . . .
. . . . . . . . o . . . . . . .
. . . . . . . . o . . . . . . .
. . . o . o . . . . . . . . . .
. . . . o . o o . . . . . . . .
. . . o . o o . . . . . . . . .
. . . . . . . . . . . . . . . .
""")

def init_screen():
    wn.delay(0)
    wn.tracer(0, 0)
    wn.title("Conwoy Game of Life")

    t.penup()
    t.speed(0)
    t.ht()
    console_tui(test_grid)

init_screen()
t.onclick(click_the_turtle)
turtle.done()
