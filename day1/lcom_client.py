from lcom import main, command
import os

@command
def listing(*args, **kwargs):
    if 'dir' not in kwargs:
        print 'Error! usage: --dir DIRNAME'
        return

    print os.listdir(kwargs['dir'])

@command
def parsing():
    print 'from parsing with love'

main()
