def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

#----- testing code below ---
def test_gcd():
    assert gcd(84, 60) == 12
    assert gcd(1, 2) == 1
    assert gcd(4080, 1547) == 17

if __name__ == '__main__':
    test_gcd()
