from lcom import main, command
from simpledb import load_from_json

import os
import uuid
from shutil import copyfile
import imghdr
import json

root_dir = './image_storage'
db = load_from_json('./xxx_db1.json')


def new_guid():
    return uuid.uuid4()


def add_images_to_gallery(gallery, src_path):
    """
    check if src is file or folder
    copy the found image file and add it to the gallery
    :param gallery:
    :param src_path:
    :return:
    """
    if os.path.isfile(src_path):
        if imghdr.what(src_path) not in ['png', 'jpg', 'gif', 'jpeg']:
            print 'Error! Not an allowed image file'
            return
        new_image_name = str(new_guid())
        dir_path = os.path.join(root_dir, new_image_name[0])
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        new_image_path = os.path.join(dir_path, new_image_name)
        db.add_catalog(gallery, {new_image_name: new_image_path})
        copyfile(src_path, new_image_path)
        db.commit()
        print 'Done'
        return
    if os.path.isdir(src_path):
        for dir_file in os.listdir(src_path):
            add_images_to_gallery(gallery, os.path.join(src_path, dir_file))

        print 'Done'
        return


@command
def copy(*args, **kwargs):
    if 'path' not in kwargs:
        print 'Error! usage: --path /path/to/file'
        return
    if 'gallery' not in kwargs:
        print 'Error! must specify gallery'
        return

    return add_images_to_gallery(kwargs['gallery'], kwargs['path'])


@command
def dump(*args, **kwargs):
    if 'gallery' not in kwargs:
        print 'Error! usage: --gallery gallery_name'
        return

    print json.dumps(db.get_catalog(kwargs['gallery']))

main()
