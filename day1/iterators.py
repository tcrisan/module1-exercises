# coding=utf-8
import codecs
def non_empty_lines(file):
    for line in file:
        if line.strip():
            yield line

def numbered_lines(lines):
    for i, line in enumerate(lines):
        yield "%d:%s" % (i, line)

def wrap_at_column(lines, w):
    # yields lines
    pass

def paginate(lines, page_size, page_separator='|'):
    for i, line in enumerate(lines):
        if i % page_size == 0:
            yield page_separator
        else:
            yield line

def print_lines(lines):
    for l in lines:
        print l


lines = codecs.open('./file.txt', encoding='utf-8')
print_lines(numbered_lines(non_empty_lines(paginate(lines, 3))))