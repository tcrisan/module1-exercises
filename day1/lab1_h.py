# Homework problems for unit 1
# ----------------------------

#The rest of dividing a square number like 81, by 32 can only be
#0, 1, 4, 9, 16, 17, 25
#A number that has such a rest when divided by 32 is called a pseudo square mod 32
def pseudo_square_mod32(n):
    remainders_list = [0, 1, 4, 9, 16, 17, 25]
    return True if n % 32 in remainders_list else False

def certain_square(n):
    return (n**0.5 + 0.5)**2 == n

# Test how useful is the maybe_square test.
# How many false positives it reports for numbers under 1000?
# What percentage of tests are false positives?
def maybe_square_accuracy():
    N = 10000
    pass

# Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.
def remove_adjacent(nums):
    nums_map = {num_value: 1 for num_value in nums}
    return sorted(nums_map.keys())


# rescale transforms the range fm..fM to tm..tM
# Example: if f iterates from 0 to 10 and tm, tM == 0, 1
# then rescale(f) should be 0.0, 0.1, 0.2 ... 1.0
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    if tm == 0 and tM == 1:
        f = (f - fm)/(fM - fm)
    else:
        f *= (tM - tm) / (fM - fm)
    return f

def test_pseudo_square_mod32():
    assert pseudo_square_mod32(9)
    assert not pseudo_square_mod32(24)
    assert pseudo_square_mod32(17)

def test_remove_adjacent():
    assert remove_adjacent([1, 2, 2, 3]) == [1, 2, 3]
    assert remove_adjacent([2, 2, 3, 3, 3]) == [2, 3]
    assert remove_adjacent([]) == []

def test_rescale():
    assert rescale(5, 0, 10, 0, 1) == 0.5
