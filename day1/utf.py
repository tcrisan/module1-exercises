# coding=utf-8
import urllib2
import codecs
f = codecs.open('./file.txt', 'a', encoding='utf-8')

encoded = codecs.encode(u'http://ko.wikipedia.org/wiki/위키백과:대문', 'utf-8')
url = urllib2.urlopen(encoded)

for i, line in enumerate(url):
    f.write(u"%d : %s" % (i, codecs.decode(line, 'utf-8')))

f.close()