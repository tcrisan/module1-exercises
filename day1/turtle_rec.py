import random
import turtle

import time

t = turtle.Turtle()
delta = 10

def it_spiral(r):
    for r in xrange(r, 0, -delta):
        t.right(90)
        t.forward(r)

def rec_spiral(r):
    if r <= 0:
        return
    t.right(90)
    t.forward(r)
    rec_spiral(r - delta)

def draw_circle(radius):
    if radius > 5:
        t.circle(radius)
        radius *= 0.7
        x, y = t.pos()
        t.sety(y + radius/2)

        draw_circle(radius)
    else:
        return

a = 30  
s = 0.7
t.left(90)
def tree(r):
    if r <= 15:
        return
    t.forward(r)
    t.left(a)
    tree(r*s)
    t.right(60)
    tree(r*s)
    t.left(a)
    t.back(r)

def draw_house(r):
    t.forward(r)
    t.left(90)
    t.forward(r)
    t.left(30)
    t.forward(r)
    t.left(120)
    t.forward(r)
    t.left(120)
    t.forward(r)
    t.back(r)
    t.right(90)
    t.forward(r)
    t.left(90)
    t.forward(30)
    t.left(90)
    t.forward(25)
    t.right(90)
    t.forward(20)
    t.right(90)
    t.forward(25)


def draw_list(lst, highlight=0):
    t.clear()
    t.home()
    for i, a in enumerate(lst):
        if i == highlight:
            t.fillcolor('red')
        else:
            t.fillcolor('blue')
        t.begin_fill()
        t.forward(10 * a)
        t.left(90)
        t.forward(10)
        t.left(90)
        t.forward(10 * a)
        t.left(90)
        t.forward(10)
        t.end_fill()
        t.forward(15)
        t.left(90)

def koch(order, size):
    if order == 0:
        t.forward(size)
    else:
        for angle in [60, -120, 60, 0]:
            koch(order-1, size/3)
            t.left(angle)

def koch_all(l, i):
    for i in xrange(3):
        koch(l, i)
        t.right(120)

def animated_insort(lst):
    turtle.tracer(0, 0) # disable turtle tracing
    pass

def run_tree():
    tree(150)
    turtle.done()

def run_house():
    draw_house(80)
    turtle.done()

def run_koch():
    koch(5, 1000)
    turtle.done()

def run_spirals():
    rec_spiral(180)
    t.up()
    t.goto(50, 50)
    t.down()
    it_spiral(100)
    turtle.done()

def run_list_show():
    lst = range(1, 10)
    random.shuffle(lst)
    draw_list(lst)
    turtle.done()

def run_animated_insort():
    #run_list_show()
    #lst = range(10, 1, -1)
    lst = [random.uniform(2, 15) for _ in xrange(18)]
    animated_insort(lst)

def run_draw_circle():
    draw_circle(300)
    turtle.done()

# run_draw_circle()
run_tree()
# run_koch()