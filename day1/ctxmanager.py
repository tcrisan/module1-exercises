from contextlib import contextmanager
import json
import datetime

@contextmanager
def div_context():
    print '<div>'
    yield
    print '</div>'


def a_href(href):
    with div_context():
        print '<a href="%s"/>' % href

# a_href('amazon')


#json decorator

def to_json(func):
    def wrapper(*args, **kwargs):
        return json.dumps(func(*args, **kwargs))

    return wrapper


@to_json
def listamea(l):
    return l

# print listamea([{'cheie': 'valoare', 'numar': 5}, {'cheie': 'valoare', 'numar': 4}])

#validator

def validate(func):
    def wrapper(*args, **kwargs):

        for arg in args:
            if isinstance(arg, dict):
                for key, value in arg.iteritems():
                    if not isinstance(key, unicode) or not isinstance(value, unicode):
                        raise TypeError('There was a type error on dict param')
            elif isinstance(arg, list):
                for i, value in enumerate(arg):
                    if not isinstance(value, unicode) or not isinstance(value, unicode):
                        raise TypeError('There was a type error on list param')
            else:
                if type(arg) != unicode:
                    raise TypeError('There was a type error on scalar param')

        ret = func()

        if isinstance(ret, tuple):
            for t in ret:
                if type(t) != unicode:
                    raise TypeError('There was a type error on return tuple')

        if type(ret) != unicode:
            raise TypeError('There was a type error on return value')

        return func()

    return wrapper

@validate
def test_list():

    return (u'Brici!', 5)

#test_list(u'sss', {5:7})
#test_list(u'sss', [5,7])
#test_list(u'sss', 4,6,7,8)
#test_list(u'sss', u'aaa', u'bbb')

@contextmanager
def add_date(file_path, action):
    if action not in ['w', 'a']:
        raise TypeError('Only write/append mode allowed')

    r = open(file_path, 'r')
    content = r.read()
    r.close()

    today = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '\n'
    f = open(file_path, 'w')
    format_content = today + content + '\n' if action == 'a' else today
    print format_content
    f.write(format_content)
    f.close()

    a = open(file_path, 'a')
    yield a
    a.close()

with add_date('./date.txt', 'w') as file_handler:
    file_handler.write('gica are mere')




