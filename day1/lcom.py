# coding=utf-8

import sys
import re


def _parse_line(cli_args_str):
    """
    >>> _parse_line('ana are')
    (['ana', 'are'], {})
    >>> _parse_line('ana --are 4')
    (['ana'], {'are': '4'})
    >>> _parse_line('--are 4 ana')
    Traceback (most recent call last):
    ...
    ValueError: positional arg after keyword arg
    >>> _parse_line('--34 3')
    Traceback (most recent call last):
    ...
    ValueError: keyword arg must start with letter
    >>> _parse_line('ana --are')
    Traceback (most recent call last):
    ...
    ValueError: missing keyword arg value

    test: python lcom.py pos1 --key1 10 --key2 20
    """

    cli_args = cli_args_str.split(' ')
    positional_args = []
    keyword_args = {}
    skip_loop = False
    keyword_pattern = '^--'

    for i, arg in enumerate(cli_args):
        if skip_loop:
            skip_loop = False
            continue
        keyword_match = re.match(keyword_pattern, arg)

        if not keyword_match:
            positional_args.append(arg)
            skip_loop = False
            continue

        try:
            '''dies if not args[i+1]'''
            value = cli_args[i+1]
            keyword = str(arg).replace('--', '')
        except Exception:
            raise ValueError('missing keyword arg value')

        if not re.match('^--[A-Za-z]', arg):
            raise ValueError('keyword arg must start with letter')

        try:
            '''dies if not args[i+2](controlled)'''
            positional_arg_attempt = cli_args[i+2]
            if not re.match(keyword_pattern, positional_arg_attempt):
                raise ValueError
        except IndexError:
            keyword_args[keyword] = value
            return positional_args, keyword_args
        except Exception:
            raise ValueError('positional arg after keyword arg')

        keyword_args[keyword] = value
        skip_loop = True

    return (positional_args, keyword_args)


_registry = {}


def command(func):
    _registry[func.__name__] = func
    return func


def main():
    command_line_args = sys.argv[1:]
    pos_args, key_args = _parse_line(' '.join(command_line_args))
    func_name = pos_args[0]
    call_args = pos_args[1:]

    _registry[func_name](*call_args, **key_args)