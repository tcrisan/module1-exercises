from contextlib import contextmanager
import json
from codecs import open


class SimpleDb(object):
    def __init__(self, path):
        self.path = path
        try:
            with open(path) as f:
                self.db = json.load(f)
        except IOError:
            self.db = {'catalogs': {}}
        self.sane()

    def sane(self):
        if not self.db:
            return
        if 'catalogs' not in self.db:
            raise ValueError('bad db')
        if type(self.db['catalogs']) is not dict:
            raise ValueError('bad db')

    @property
    def catalogs(self):
        return self.db['catalogs']

    def add_catalog(self, catalog, images):
        if catalog not in self.catalogs:
            self.catalogs[catalog] = images
        else:
            for k, v in images.iteritems():
                self.catalogs[catalog][k] = images[k]

    def get_catalog(self, cat):
        if cat not in self.catalogs:
            self.catalogs[cat] = {}
        return self.catalogs[cat]

    def commit(self):
        self.sane()
        with open(self.path, 'w') as f:
            json.dump(self.db, f)

def load_from_json(path):
    return SimpleDb(path)


# @contextmanager
# def process_json_file(path, action, db=None):
#     f = open(path, action, encoding='utf-8')
#     if action == 'r':
#         yield json.loads(f.read())
#     else:
#         f.write(json.dumps(db))
#         yield True
#     f.close()
#
#
# def load_from_json(path):
#     with process_json_file(path, 'r') as data:
#         return data
#
#
# def save_to_json(path, db):
#     if not db:
#         return False
#     with process_json_file(path, 'w', db) as success:
#         return success
#
# print load_from_json('./db.json')
