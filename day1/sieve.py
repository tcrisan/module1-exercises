# this should be an implementation of the sieve of eratosthenes
# however it is wrong. FIx it. Use a debugger or print intermediate results.
def sieve(upto):
    sv = [True for _ in xrange(upto)]
    for a in range(2, int(upto**0.5)):
        if sv[a]: # a prime
            for i in xrange(a, upto, a):
                sv[i] = False
    return sv[2:]

def primes(upto):
    return [i+2 for i, prime in enumerate(sieve(upto)) if prime]

def test_primes():
    assert primes(10) == [2, 3, 5, 7]
    assert primes(20) == [2, 3, 5, 7, 11, 13, 17, 19]
